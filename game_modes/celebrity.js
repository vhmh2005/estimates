module.exports = function(app) {
	var config = require('../config')(),
        mongo = config.mongo,
        host = config.host,
        port = config.port,
				mongoClient = require('mongodb').MongoClient;

	app.post('/get_questionceleb', function(req, res) {
	    getQuestionCeleb(function(question) {
	        res.json(question);
	    });
	});
	
	function getQuestionCeleb(fn){
	        mongoClient.connect("mongodb://" + mongo.hostname + ":" + mongo.port + "/" + mongo.db, function(err, db) {
	        if (err) {
	            fn({error: {message: "cannot connect to database"}});
	            return;
	        }

	        var collection = db.collection('celebrity');

	        // iterate through all items in database
	        collection.find().toArray(function(err, items) {
	            if (err) {
	                fn({error: {message: "cannot retrieve items from db"}});
	                return;
	            }

	            var value = Math.floor((Math.random()*(items.length)));
	            var question = items[value];
	            var type_question =Math.floor((Math.random()*(3)+1));
	            var lquestion ={"celeb":question.celeb,"img_url":question.img_url,"question":"","answer":"","unit":"","mode_question":"celebrity"};
	            var birthday = (question.dob).split("/");
	            switch(type_question){
	                case 1:
	                    lquestion.answer = question.weight;
	                    lquestion.question ="How much does "+question.celeb+" weigh?";
	                    lquestion.unit = 'Kg';
	                    break;
	                case 2:
	                  lquestion.answer = question.height;
	                  lquestion.question ="How tall is "+question.celeb+" ?"
	                   lquestion.unit = 'cm';
	                    break;
	                case 3:
	                    lquestion.answer = calculateAge(birthday[2]+","+birthday[1]+","+birthday[0]);
	                    lquestion.question ="How old is "+question.celeb+" ?"
	                       lquestion.unit = 'age';
	                    break;
	            }
	            fn(lquestion);
	        });
	    });
	}

	function calculateAge(birthday) { // birthday is a date
	    var today = new Date();
	    var birthDate = new Date(birthday);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age--;
	    }
	    return age;
	}

}
