module.exports = function (app) {
  var config = require('../config')(),
    mongo = config.mongo,
    host = config.host,
    port = config.port,
    mongoClient = require('mongodb').MongoClient,
    UPPERBOUND_ANSWER_LIMIT = 1000000,
    LOWERBOUND_ANSWER_LIMIT = 10,
    NUM_QUESTION = 9,
    YOUWIN = 1,
    OPPONENTWIN = 2,
    EQUAL = 3;

  var fs = require('fs'),
    path = require('path'),
    pathImage = path.join(process.cwd(), '/public/images');

  app.post('/get_question', function (req, res) {
    getQuestion(req.body.from, req.body.to, function (question) {
      res.json(question);
    });
  });


  function getQuestion(from, to, fn) {
    mongoClient.connect("mongodb://" + mongo.hostname + ":" + mongo.port + "/" + mongo.db, function (err, db) {
      if (err) {
        fn({
          error: {
            message: "cannot connect to database"
          }
        });
        return;
      }

      var collection = db.collection('items');

      // iterate through all items in database
      collection.find().toArray(function (err, items) {
        if (err) {
          fn({
            error: {
              message: "cannot retrieve items from db"
            }
          });
          return;
        }

        var questionSet = [],
          count = 0;

        items.forEach(function (doc) {
          var query = {
            type: doc.type,
            $and: [
              {
                measure: {
                  $gte: doc.measure * parseFloat(from)
                }
              },
              {
                measure: {
                  $lte: doc.measure * parseFloat(to)
                }
              }
            ]
          };

          // iterate through second items that match query
          collection.find(query).toArray(function (err, comparableItems) {
            if (comparableItems && comparableItems.length > 0) {
              // found new set of questions
              questionSet.push({
                firstThing: doc,
                secondThings: comparableItems
              });
            }
            count++;
            if (count === items.length) {
              // done searching for questions
              if (questionSet.length === 0)
                fn({
                  error: {
                    message: "cannot find the question"
                  }
                });
              else {
                var firstItemIndex = Math.floor(Math.random() * questionSet.length),
                  secondItemIndex = Math.floor(Math.random() * questionSet[firstItemIndex].secondThings.length),
                  question = {
                    firstThing: questionSet[firstItemIndex].firstThing,
                    secondThing: questionSet[firstItemIndex]["secondThings"][secondItemIndex]
                  },
                  answer = (question.secondThing.measure / question.firstThing.measure).toFixed(0);
                question.firstThing.image = 'http://' + host + ':' + port + '/images/' + question.firstThing.id + '.jpg';
                question.secondThing.image = 'http://' + host + ':' + port + '/images/' + question.secondThing.id + '.jpg';

                fn({
                  question: question,
                  answer: answer
                });
              }
            }
          });
        });
      });
    });
  }

  var singleRooms = [], // rooms that contains exactly 1 player
  // example of rooms
    rooms = {};


  var io = require('socket.io').listen(app.listen(port)),

    stopPoint;


  io.sockets.on('connection', function (socket) {


    app.get('/get_image/:id', function (req, res) {
      console.log(req.param("id"));
      callback(req.param("id"), res);
    });

    socket.on('finish', function (data) {
      console.log('finish transfer');
    });

    function callback(id, res) {
      fs.readFile(process.cwd() + '/data.json', 'utf8', function (err, data) {
        if (err) return console.log(err);

        var database = JSON.parse(data);
        stopPoint = database.length;
        var imgStream = fs.createReadStream(pathImage + "/" + database[id].id + ".jpg");
        imgStream.pipe(res);
      });
    }

    socket.on('join', function () {
      console.log(singleRooms.length);

      if (singleRooms.length > 0) {
        // there is a single room
        room = singleRooms.pop();
        console.log('new socket joining ' + room);
        socket.join(room);
        socket.emit('joined', room);
        io.sockets.in(room).emit('start');
        rooms[room].playersCount = 2;
        rooms[room].status = "playing";
        socket.set('player_id', 1);
      } else {
        // create new room
        room = 'room_' + Math.floor(Math.random() * 1000000);
        console.log('This is room of both player: ' + room);
        rooms[room] = new Room(1);
        singleRooms.push(room);
        console.log('new socket joining ' + room);
        socket.join(room);
        socket.emit('joined', room);
        socket.set('player_id', 0);
      }

      socket.set('room', room);

    });

    socket.on('get question', function (roundId) {

      console.log('This is round is from server: ' + roundId);


      from = LOWERBOUND_ANSWER_LIMIT;
      to = UPPERBOUND_ANSWER_LIMIT;

      socket.get('room', function (err, room) {
        if (roundId >= NUM_QUESTION) {
          console.log('This is emit of end game');
          io.sockets.in(room).emit('end game');
          return;
        }
        if (rooms[room] != undefined) {
          round = rooms[room].rounds[roundId];
          if (++round.questionRequests >= 2) {
            getQuestion(from, to, function (data) {
              if (typeof data.error !== 'undefined') {
                console.log(data);
              } else {
                round.question = data.question;
                round.answer = data.answer;
                console.log("New question generated ", data.question.firstThing.subject + " : " + data.question.secondThing.subject);
                console.log('answer: ' + data.answer);
              }
              io.sockets.in(room).emit('question', data);
            });
          }
        }
      });
    });

    socket.on('player_data', function (data) {
      socket.broadcast.to(room).emit('opponent info', data);
    });

    socket.on('disconnect', function () {
      leaveRoom();
    });
    socket.on('leave', leaveRoom);

    function leaveRoom() {

      socket.get('room', function (err, room) {
        console.log("Room Leave : " + room);
        if (rooms[room] != undefined) {
          if (typeof room === 'string') {
            socket.leave(room);
            socket.emit('left');
            socket.broadcast.to(room).emit('opponent left room');
            delete rooms[room];
            index = singleRooms.indexOf(room);
            if (index !== -1)
              singleRooms.splice(index, 1);
          }
        }
      });
    }

    socket.on('answer', function (roundId, answer) {
      console.log('This is number question: ' + roundId);
      socket.get('room', function (err, room) {
        if (rooms[room] != undefined) {
          console.log('room status:' + room);
          console.log('room round' + rooms[room]);
          round = rooms[room].rounds[roundId];

          console.log("round :" + round);
          socket.get('player_id', function (err, id) {
            console.log('This is id of player: ' + id);
            round.answerSubmitted[id] = answer;
            if (typeof round.answerSubmitted[1 - id] !== 'undefined') {
              // calculate results
              round = rooms[room].rounds[roundId];
              answers = round.answerSubmitted;
              correctAnswer = round.answer;
              points = rooms[room].points;
              yourPercent = percentOff(answers[id], correctAnswer);
              oppPercent = percentOff(answers[1 - id], correctAnswer);
              if (yourPercent < oppPercent)
                points[id]++;
              else if (yourPercent > oppPercent)
                points[1 - id]++;
              io.sockets.in(room).emit('both answered');
            }
          });
        }
      });
    });

    socket.on('get result', function (roundId) {
      socket.get('room', function (err, room) {
        round = rooms[room].rounds[roundId];
        answers = round.answerSubmitted;
        correctAnswer = round.answer;
        points = rooms[room].points;
        socket.get('player_id', function (err, id) {
          result = {
            correctAnswer: correctAnswer,
            yourPercent: percentOff(answers[id], correctAnswer),
            oppPercent: percentOff(answers[1 - id], correctAnswer),
            yourPoints: points[id],
            oppPoints: points[1 - id],
            answerFirst: answers[id],
            answerSecond: answers[1 - id]
          };
          if (result.yourPercent < result.oppPercent)
            result.winner = YOUWIN;
          else if (result.yourPercent > result.oppPercent)
            result.winner = OPPONENTWIN;
          else
            result.winner = EQUAL;

          socket.emit('result', result);
        });
      });
    });
  });


  function Room(playersCount) {
    this.rounds = new Array();
    this.playersCount = playersCount;
    this.status = "waiting";
    this.points = [0, 0];
    for (i = 0; i < NUM_QUESTION; i++) {
      this.rounds.push({
        "questionRequests": 0,
        "answerSubmitted": new Array(),
        "question": {},
        "answer": null
      });
    }
  }

  function percentOff(a, b) {
    return parseInt(Math.abs((parseFloat(a) - b) / b * 100).toFixed(0));
  }
}