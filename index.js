/**
 * Created by hungvo on 2/27/14.
 */
// Retrieve
var express = require('express'),
    app = express()
        .use(express.bodyParser())
        .use(express.static('public'));

console.log("listening on port ", require('./config')().port);

require('./game_modes/estimate')(app);
require('./offline_data_update')(app);
require('./user')(app);
