module.exports = function(app) {
	var config = require('./config')(),
	mongo = config.mongo,
	host = config.host,
	port = config.port,
	mongoClient = require('mongodb').MongoClient;

	app.post('/add_user',function(req,res){
		addUser(req, res);
	})

	function addUser(req, res) {
		mongoClient.connect("mongodb://" + mongo.hostname + ":" + mongo.port + "/" + mongo.db, function(err, db) {
			if (err) {
				res.send(false);
				return;
			}

			db.createCollection('user', {w:1}, function(err, collection) {
				if (err) {
					res.send(false);
					return;
				}
				else {
					var doc = {'name': req.body.name, 'birthday': req.body.birthday, 'email': req.body.email,'password':req.body.password};

					collection.insert(doc, {w:1}, function(err, result) {
						if(err){
							res.send(false);
							return;
						}
						res.send(true);
					});
				}
			});
		});
	}

	app.post("/check_user", function(req, res){
		checkUser(req, res);
	});

		function checkUser(req, res){
			var error_status = '0', error_not_found = '1', success_found = '2';
			mongoClient.connect("mongodb://" + mongo.hostname + ":" + mongo.port + "/" + mongo.db, function(err, db) {
				if (err)
						res.send(error_status);
				var collection = db.collection('user');

				collection.find().toArray(function(err, items){
					if(err)
							res.send(error_status);
				var flag = error_not_found;

				for(var i = 0;i < items.length; i++){
					if(items[i].email == req.body.email && items[i].password == req.body.password){
						flag = success_found;
						break;
					}
			}

			res.send(flag);

		});
	});
	}
}
