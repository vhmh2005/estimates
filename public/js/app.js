var AppRouter = Backbone.Router.extend({
    game_mode : 'estimate',
    routes: {
        "": "getQuestion",
        "question": "showQuestion",
        "answer": "showAnswer",
        "celebrity":"getQuestion_celeb",
    },
    initialize: function() {
        this.questionModel = new QuestionModel();
        this.answerModel = new AnswerModel();
        this.mainView = new MainView();
        this.questionView = new QuestionView({model: this.questionModel});
        this.answerView = new AnswerView({model: this.answerModel});
        this.questionCelebModel = new QuestionCelebModel();
        this.questionCelebView = new QuestionCelebView({model: this.questionCelebModel});
    },
    getQuestion: function() {
        app.game_mode = 'estimate';
        this.render();
    },
    showAnswer: function() {
        $('#result').show();
    },
    hideAnswer: function() {
        $('#result').hide();
    },
    getQuestion_celeb:function() {
        app.game_mode = 'celebrity';
        this.render();
    },
    render: function() {
        $('#result').html(app.answerView.render().el);
        app.hideAnswer();
        if (app.game_mode == 'celebrity') {
            $('#top').empty();
            $('#question').html(app.questionCelebView.render().el);
        } else {
            $('#top').html(app.mainView.render().el);
            $('#question').html(app.questionView.render().el);      
        }
    }
})

var app = new AppRouter();

$(function() {
    Backbone.history.start();
    app.render();
});


    