/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
MainView = Backbone.View.extend({
    templateFile: "/templates/main.html",
    templateString: "",
    initialize: function() {
        $.get(this.templateFile, this.setTemplateString.bind(this));
    },
    setTemplateString: function(data) {
        this.templateString = data;
        this.render();
        this.getQuestion();
    },
    render: function() {
        this.$el.html(this.templateString);
        this.delegateEvents();
        return this;
    },
    events: {
        'click #get_question': function(e) {
            e.preventDefault();
            this.getQuestion();
        }
    },
    getQuestion: function() {
        $.ajax({
            type: 'POST',
            url: '/get_question',
            data: {
                from: this.$('#from').val(),
                to: this.$('#to').val()
            },
            dataType: 'json'
        }).done(function(data) {
            app.questionModel.set(data);
            app.hideAnswer();
        })
    }
})

