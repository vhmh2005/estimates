/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

QuestionView = Backbone.View.extend({
    templateFile: "/templates/question.html",
    templateString: "",
    initialize: function() {
        this.listenTo(this.model, "change", this.render);
        $.get(this.templateFile, this.setTemplateString.bind(this));
    },
    setTemplateString: function(data) {
        this.templateString = data;
    },
    render: function() {
        this.$el.html(Mustache.render(this.templateString, this.model.attributes));
        this.delegateEvents();
        return this;
    },
    events: {
        'click #check_answer': 'checkAnswer'
    },
    checkAnswer: function(e) {
        if (e)
            e.preventDefault();
        answer = app.questionModel.get('answer');
        app.answerModel.set({
            answer: answer,
            percent: (Math.abs((this.$('#answer').val() - answer) / answer) * 100).toFixed(0)
        });
        app.showAnswer();
    }
})
