/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

AnswerView = Backbone.View.extend({
    templateFile: "/templates/answer.html",
    templateString: "",
    initialize: function() {
        this.listenTo(this.model, "change", this.render);
        $.get(this.templateFile, this.setTemplateString.bind(this));        
    },
    setTemplateString: function(data) {
        this.templateString = data;
    },
    render: function() {
        this.$el.html(Mustache.render(this.templateString, this.model.attributes));
        this.delegateEvents();
        return this;
    },
    events: {
        'click #new_question': function() {
            if(app.game_mode ==='celebrity'){
                app.questionCelebView.getQuestionCeleb();
            }
            else{
                app.mainView.getQuestion();
            }
        }
    }
})
