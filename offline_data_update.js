module.exports = function(app) {
	var config = require('./config')(),
	mongo = config.mongo,
	host = config.host,
	port = config.port,
	mongoClient = require('mongodb').MongoClient,
	exec = require('child_process').exec;

	var stream = require('stream'),
	    util = require('util'),
			crypto = require('crypto'),
			fs = require('fs'),
			path = require('path'),
			sys = require('sys'),
			pathImage = path.join(process.cwd(), '/public/images/');


	// Get data exported from mongodb
	app.get('/get_data', function(req, res) {
		var query = "mongoexport -db " + mongo.db + " -c items --jsonArray -o data.json";
		console.log(query);
		var child = exec(query,
			function (error, stdout, stderr) {
				if (error !== null) {
					console.log('export query failed: ' + error);
				}
				res.sendfile("data.json");
			});
	});

	//GET IMAGE

	// app.get('/send_image', function(req, res){
	//
	// 	var originBuffer = new Buffer('');
	//
	//
	//
	// 	fs.readFile(process.cwd() + '/data.json', function (err,data) {
	// 			if (err) return console.log(err);
	//
	// 			var database = JSON.parse(data);
	//
	// 			for(var i = 0;i < database.length;i++) {
	// 				var id = database[i].id.toString();
	// 				fs.stat(pathImage + id + '.jpg', function (err, stats) {
	// 						storage(id, stats.size);
	// 				});
	// 			}
	// 			console.log(originBuffer.length);
	//
	// 	});
	//
	// 		function storage(name, size) {
	// 			var buffer = crypto.randomBytes(size);
	// 			var wstream = fs.createWriteStream(pathImage + name + ".jpg");
	// 			wstream.write(buffer);
	// 			originBuffer = Buffer.concat([originBuffer, buffer]);
	// 		}
	// });

	// Check whether database has been changed since the last time client has updated db
	app.post('/check_data', function(req,res){
		mongoClient.connect("mongodb://" + mongo.hostname + ":" + mongo.port + "/" + mongo.db, function(err, db) {
			if (err) {
				res.send('');
			} else {
				var collection = db.collection('items');
				collection.find().toArray(function(err, items) {
					if(err){
						res.send('');
					} else {
						var result = items.sort(function(doc1, doc2){
							return doc1._id.getTimestamp() - doc2._id.getTimestamp();
						});
						var lastUpdatedTime = "" + result[0]._id.getTimestamp();
						console.log("Last updated time on server: " + lastUpdatedTime);
						if (req.body.lastUpdatedTime) {
							console.log("Last updated time on client: " + req.body.lastUpdatedTime);
							if (lastUpdatedTime != req.body.lastUpdatedTime) {
								console.log('need to update data on client');
								res.send(lastUpdatedTime);
							} else {
								// no need updated
								console.log('no need to update data on client');
								res.send('');
							}
						} else {
							console.log("Client does not have offline data");
							// need updated
							console.log('need to update data on client');
							res.send(lastUpdatedTime);
						}
					}
				});
			}
		});
	});
}
