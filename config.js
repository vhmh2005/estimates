/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = function() {
    var dev_settings = {
        mongo: {
            hostname: 'localhost',
            port: '27017',
            db: 'estimates'
        },
        host:'localhost',
        port: 1337
    }, prod_settings = {
        mongo: {
            hostname: 'localhost',
            port: '27017',
            db: 'estimates'
        },
        host:'188.226.159.206',
        port: 88
    };
    switch (process.env.NODE_ENV) {
        case 'development':
            return dev_settings;
            break;
        case 'production':
            return prod_settings;
            break;
        default:
            return dev_settings;
    }
};
